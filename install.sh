#!/bin/bash
echo "----- SSH KEYS -----"
ssh-keygen -lf /etc/ssh/ssh_host_ecdsa_key.pub
ssh-keygen -lf /etc/ssh/ssh_host_ed25519_key.pub
sudo ssh-keygen -lf /etc/ssh/ssh_host_dsa_key.pub
sudo ssh-keygen -lf /etc/ssh/ssh_host_rsa_key.pub
echo "----- SSH KEYS -----" && exit